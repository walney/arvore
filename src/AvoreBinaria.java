
public class AvoreBinaria {
	No raiz;
	int inseridos;
	public void inserir(No elementos) {
		if(raiz==null) {
			raiz=elementos;
		}
		else {
			No novo=raiz;
			inserir(novo,elementos);
		}
		inseridos++;
		
	}
	public void inserir(No novo, No elementos) {
		if(novo.valor>elementos.valor) {
			if(novo.esquerda ==null) {
				novo.esquerda=elementos;
				
			}
			else {
				No proxNo= novo.esquerda;
				inserir(proxNo,elementos);
				
			}
		}
		else{
			if(novo.direita ==null) {
				novo.direita=elementos;
				
			}
			else {
				No proxNo= novo.direita;
				inserir(proxNo,elementos);
				
			}
			
			
		}
		
	}
	public void imprimirEmOrdem() {
		emOrdem(raiz);
	}
	public void emOrdem(No avore) {
		if(avore==null)return;
		emOrdem(avore.esquerda);
		System.out.print("___"+avore.valor+"____");
		
		emOrdem(avore.direita);
	}
	
	
	
	

}
